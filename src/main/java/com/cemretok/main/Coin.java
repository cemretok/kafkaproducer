package com.cemretok.main;

import java.io.Serializable;

//https://github.com/ververica/flink-training-exercises/blob/master/src/main/java/com/ververica/flinktraining/exercises/datastream_java/datatypes/TaxiRide.java

public class Coin implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1833301510600806464L;
	public String name;
	public Double price;
	public Long time;

	public Coin(String name, Double price, Long time) {
		super();
		this.name = name;
		this.price = price;
		this.time = time;
	}

	public Coin() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return name + "," + price + "," + time;
	}

	public static Coin fromString(String line) {

		String[] tokens = line.split(",");
		if (tokens.length != 3) {
			throw new RuntimeException("Invalid record: " + line);
		}

		Coin coin = new Coin();

		try {
			coin.name = tokens[0].toString();
			coin.price = Double.parseDouble(tokens[1]);
			coin.time = Long.parseLong(tokens[2]);
		} catch (NumberFormatException nfe) {
			throw new RuntimeException("Invalid record: " + line, nfe);
		}

		return coin;
	}

}

package com.cemretok.main;

import java.util.List;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import com.binance.api.client.BinanceApiCallback;
import com.binance.api.client.domain.event.AllMarketTickersEvent;

public class WebSocketProducer {
	private ClientFactory cf;
	private Properties configProperties;
	private Producer<String, Coin> producer;

	public WebSocketProducer() {

	}

	public WebSocketProducer(ClientFactory cf) {
		this.cf = cf;
	}

	public void loadConfiguration() {
		configProperties = new Properties();
		configProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9093");
		configProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		configProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "com.cemretok.main.CoinSchema");
	}

	public void createProducer() {
		loadConfiguration();
		producer = new KafkaProducer<String, Coin>(configProperties);
	}

	public void sendKafka(String key, Coin coin) {
		try {
			producer.send(new ProducerRecord<String, Coin>(key, coin));
			System.out.println(key + " | " + coin.toString());
		} catch (Exception e) {
			System.err.println("sendKafka failed");
			e.printStackTrace();
		}
	}

	public void listen() {
		createProducer();

		cf.getWebSocketClientFactory().onAllMarketTickersEvent(new BinanceApiCallback<List<AllMarketTickersEvent>>() {
			@Override
			public void onResponse(List<AllMarketTickersEvent> response) {
				response.stream().forEach(k -> {
					sendKafka(k.getSymbol(),
							new Coin(k.getSymbol(), Double.parseDouble(k.getBestBidPrice()), k.getEventTime()));
				});

			}

			@Override
			public void onFailure(final Throwable cause) {
				System.err.println("Web socket failed");
				cause.printStackTrace(System.err);
				listen();
			}

		});
	}

}
